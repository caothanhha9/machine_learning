#! /usr/bin/env python
# -*- coding: utf-8 -*-
import os
import pickle

import datetime
import tensorflow as tf

from utils import logger_gen, data_helpers
from utils.constants import Config, Messages, FilePath
from text_cnn import TextCNN

logger = logger_gen.initiate_logger(__name__)


class Predict(object):
    def __init__(self, positive_path="", negative_path="", vocabulary_save_path="", out_dir="",
                 set_flag=False, split_index=-5, learning_rate=1e-4, embedding_dim=128,
                 filter_sizes="2,3,4,5", num_filters=128, dropout_keep_prob=0.5, l2_reg_lambda=0.0,
                 batch_size=18, num_epochs=200, evaluate_every=100, checkpoint_every=100,
                 allow_soft_placement=True, log_device_placement=False, num_of_classes=5):
        self.ckpt_dir = out_dir + "/checkpoints"
        self.config_path = out_dir + "/train.config"
        self.positive_path = positive_path
        self.negative_path = negative_path
        self.vocabulary_save_path = vocabulary_save_path
        self.out_dir = out_dir
        self.set_flag = set_flag
        self.split_index = split_index
        self.learning_rate = learning_rate
        self.embedding_dim = embedding_dim
        self.filter_sizes = filter_sizes
        self.num_filters = num_filters
        self.dropout_keep_prob = dropout_keep_prob
        self.l2_reg_lambda = l2_reg_lambda
        self.batch_size = batch_size
        self.num_epochs = num_epochs
        self.evaluate_every = evaluate_every
        self.checkpoint_every = checkpoint_every
        self.allow_soft_placement = allow_soft_placement
        self.log_device_placement = log_device_placement
        self.num_of_classes = num_of_classes

    def predict(self, sentence):
        result_list = []
        prob_list = []
        # time_str = datetime.datetime.now().isoformat()
        # logger.info("-" * 50)
        # logger.info("Started method")
        # logger.info(time_str)
        # Parameters
        # ==================================================

        # Data Preparation
        # ==================================================
        vocabulary_path = open(self.vocabulary_save_path, 'rb')
        vocabulary = pickle.load(vocabulary_path)
        vocabulary_path.close()
        # Load data
        x_test, sent_arr = data_helpers.make_data_from_string(sentence, vocabulary, config_path=self.config_path,
                                                              is_fix_sequence_length=False)
        # Predicting
        # ==================================================

        with tf.Graph().as_default():
            session_conf = tf.ConfigProto(
                allow_soft_placement=self.allow_soft_placement,
                log_device_placement=self.log_device_placement)
            sess = tf.Session(config=session_conf)
            with sess.as_default():

                cnn = TextCNN(
                    sequence_length=x_test.shape[1],
                    num_classes=self.num_of_classes,
                    vocab_size=len(vocabulary),
                    embedding_size=self.embedding_dim,
                    filter_sizes=map(int, self.filter_sizes.split(",")),
                    num_filters=self.num_filters,
                    l2_reg_lambda=self.l2_reg_lambda)

                saver = tf.train.Saver(tf.all_variables())

                # Initialize all variables

                def predict_step(x_batch, ckpt):
                    # Restores from checkpoint
                    feed_dict = {
                        cnn.input_x: x_batch,
                        cnn.dropout_keep_prob: 1.0
                    }
                    scores = sess.run(cnn.scores_softmax, feed_dict)
                    predictions = sess.run(cnn.predictions, feed_dict)
                    return [scores, predictions]
                # Generate batches
                ckpt = tf.train.get_checkpoint_state(self.ckpt_dir)
                if ckpt and ckpt.model_checkpoint_path:
                    model_checkpoint_path_arr = ckpt.model_checkpoint_path.split("/")
                    abs_model_checkpoint_path = self.ckpt_dir + '/' + model_checkpoint_path_arr[-1]
                    saver.restore(sess, abs_model_checkpoint_path)
                    # time_str = datetime.datetime.now().isoformat()
                    # logger.info("-" * 50)
                    # logger.info("Started calculate")
                    # logger.info(time_str)
                    for i in range(len(x_test)):
                        x_batch = [x_test[i]]
                        scores, predictions = predict_step(x_batch, ckpt)
                        result = predictions[0]
                        prob = scores[0][result]
                        result_list.append(result)
                        prob_list.append(prob)
                        # if (predictions == [1]) & (scores[0][1] > Config.PREDICT_PROB_LIMIT):
                        #     result = True
                        #     prob = scores[0][1]
                    # time_str = datetime.datetime.now().isoformat()
                    # logger.info("-" * 50)
                    # logger.info("Finished calculate")
                    # logger.info(time_str)
                else:
                    logger.error(Messages.CHECKPOINT_NOT_FOUND + " => " + self.out_dir)
        return result_list, prob_list, sent_arr

    def predict_all(self, sentence_list):
        total_result_list = []
        total_prob_list = []
        # Parameters
        # ==================================================

        # Data Preparation
        # ==================================================
        vocabulary_path = open(self.vocabulary_save_path, 'rb')
        vocabulary = pickle.load(vocabulary_path)
        vocabulary_path.close()
        # Load data

        x_test, total_sent_arr = data_helpers.make_data_from_string_array(
            sentence_list, vocabulary, is_a_list=True,
            config_path=self.config_path)
        # Predicting
        # ==================================================

        with tf.Graph().as_default():
            session_conf = tf.ConfigProto(
                allow_soft_placement=self.allow_soft_placement,
                log_device_placement=self.log_device_placement)
            sess = tf.Session(config=session_conf)
            with sess.as_default():
                cnn = TextCNN(
                    sequence_length=x_test[0].shape[1],
                    num_classes=self.num_of_classes,
                    vocab_size=len(vocabulary),
                    embedding_size=self.embedding_dim,
                    filter_sizes=map(int, self.filter_sizes.split(",")),
                    num_filters=self.num_filters,
                    l2_reg_lambda=self.l2_reg_lambda)

                saver = tf.train.Saver(tf.all_variables())

                # Initialize all variables

                def predict_step(x_batch, ckpt):
                    # Restores from checkpoint
                    feed_dict = {
                        cnn.input_x: x_batch,
                        cnn.dropout_keep_prob: 1.0
                    }
                    scores = sess.run(cnn.scores_softmax, feed_dict)
                    predictions = sess.run(cnn.predictions, feed_dict)
                    return [scores, predictions]
                # Generate batches
                ckpt = tf.train.get_checkpoint_state(self.ckpt_dir)
                if ckpt and ckpt.model_checkpoint_path:
                    model_checkpoint_path_arr = ckpt.model_checkpoint_path.split("/")
                    abs_model_checkpoint_path = self.ckpt_dir + '/' + model_checkpoint_path_arr[-1]
                    saver.restore(sess, abs_model_checkpoint_path)
                    for some_id_ in range(len(x_test)):
                        x_batch = x_test[some_id_]
                        result_list = []
                        prob_list = []
                        if len(x_batch) > 0:
                            scores, predictions = predict_step(x_batch, ckpt)
                            for score_id, score_ in enumerate(scores):
                                result = predictions[score_id]
                                prob = score_[result]
                                result_list.append(result)
                                prob_list.append(prob)
                        total_result_list.append(result_list)
                        total_prob_list.append(prob_list)
                else:
                    logger.error(Messages.CHECKPOINT_NOT_FOUND + " => " + self.out_dir)
        return total_result_list, total_prob_list, total_sent_arr
