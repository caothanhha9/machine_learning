#! /usr/bin/env python
# -*- coding: utf-8 -*-

import datetime
import os
import pickle

import numpy as np
import tensorflow as tf

from utils import data_helpers
from utils.constants import FilePath
from text_cnn import TextCNN


class Train(object):
    def __init__(self, positive_path, negative_path, vocabulary_save_path, out_dir, set_flag=False,
                 split_index=-5, learning_rate=1e-4, embedding_dim=128, filter_sizes="2,3,4,5",
                 num_filters=128, dropout_keep_prob=0.5, l2_reg_lambda=0.0, batch_size=18,
                 num_epochs=200, evaluate_every=100, checkpoint_every=100,
                 allow_soft_placement=True, log_device_placement=False, num_of_classes=5,
                 continue_train=False, write_summary=False,
                 node_id=-1, node_list_path=FilePath.NODE_LIST_PATH,
                 keep_balance=False):
        self.ckpt_dir = out_dir + "/checkpoints"
        self.config_path = out_dir + "/train.config"
        self.positive_path = positive_path
        self.negative_path = negative_path
        self.vocabulary_save_path = vocabulary_save_path
        self.out_dir = out_dir
        self.set_flag = set_flag
        self.split_index = split_index
        self.learning_rate = learning_rate
        self.embedding_dim = embedding_dim
        self.filter_sizes = filter_sizes
        self.num_filters = num_filters
        self.dropout_keep_prob = dropout_keep_prob
        self.l2_reg_lambda = l2_reg_lambda
        self.batch_size = batch_size
        self.num_epochs = num_epochs
        self.evaluate_every = evaluate_every
        self.checkpoint_every = checkpoint_every
        self.allow_soft_placement = allow_soft_placement
        self.log_device_placement = log_device_placement
        self.num_of_classes = num_of_classes
        self.continue_train = continue_train
        self.write_summary = write_summary
        self.node_id = node_id
        self.node_list_path = node_list_path
        self.keep_balance = keep_balance

    def run(self):
        # Parameters
        # ==================================================
        """
        if self.set_flag:
            # Model Hyperparameters
            tf.flags.DEFINE_integer("embedding_dim", self.embedding_dim, "Dimensionality of character embedding (default: 128)")
            tf.flags.DEFINE_string("filter_sizes", self.filter_sizes, "Comma-separated filter sizes (default: '3,4,5')")
            tf.flags.DEFINE_integer("num_filters", self.num_filters, "Number of filters per filter size (default: 128)")
            tf.flags.DEFINE_float("dropout_keep_prob", self.dropout_keep_prob, "Dropout keep probability (default: 0.5)")
            tf.flags.DEFINE_float("l2_reg_lambda", self.l2_reg_lambda, "L2 regularizaion lambda (default: 0.0)")

            # Training parameters
            tf.flags.DEFINE_integer("batch_size", self.batch_size, "Batch Size (default: 64)")
            tf.flags.DEFINE_integer("num_epochs", self.num_epochs, "Number of training epochs (default: 200)")
            tf.flags.DEFINE_integer("evaluate_every", self.evaluate_every, "Evaluate model on dev set after this many steps (default: 100)")
            tf.flags.DEFINE_integer("checkpoint_every", self.checkpoint_every, "Save model after this many steps (default: 100)")
            # Misc Parameters
            tf.flags.DEFINE_boolean("allow_soft_placement", self.allow_soft_placement, "Allow device soft device placement")
            tf.flags.DEFINE_boolean("log_device_placement", self.log_device_placement, "Log placement of ops on devices")
        """

        # FLAGS = tf.flags.FLAGS
        """
        FLAGS.batch_size
        print("\nParameters:")
        for attr, value in sorted(FLAGS.__flags.iteritems()):
            print("{}={}".format(attr.upper(), value))
        print("")
        """

        # Data Preparation
        # ==================================================

        # Load data
        print("Loading data...")
        x, y, vocabulary, vocabulary_inv = data_helpers.load_data(self.positive_path, self.negative_path)
        vocabulary_path = open(self.vocabulary_save_path, 'wb')
        pickle.dump(vocabulary, vocabulary_path)
        vocabulary_path.close()
        # Randomly shuffle data
        np.random.seed(10)
        shuffle_indices = np.random.permutation(np.arange(len(y)))
        x_shuffled = x[shuffle_indices]
        y_shuffled = y[shuffle_indices]
        # Split train/test set
        # TODO: This is very crude, should use cross-validation
        if self.split_index == 0:
            x_train, x_dev = x_shuffled, x_shuffled
            y_train, y_dev = y_shuffled, y_shuffled
        else:
            x_train, x_dev = x_shuffled[:self.split_index], x_shuffled[self.split_index:]
            y_train, y_dev = y_shuffled[:self.split_index], y_shuffled[self.split_index:]
        print("Vocabulary Size: {:d}".format(len(vocabulary)))
        print("Train/Dev split: {:d}/{:d}".format(len(y_train), len(y_dev)))

        # Training
        # ==================================================

        with tf.Graph().as_default():
            session_conf = tf.ConfigProto(
              allow_soft_placement=self.allow_soft_placement,
              log_device_placement=self.log_device_placement)
            sess = tf.Session(config=session_conf)
            with sess.as_default():
                if not os.path.exists(self.out_dir):
                    os.makedirs(self.out_dir)
                sequence_length = x_train.shape[1]
                f = open(self.config_path, 'w')
                f.write('sequence length: ' + str(sequence_length))
                f.close()
                cnn = TextCNN(
                    sequence_length=sequence_length,
                    num_classes=self.num_of_classes,
                    vocab_size=len(vocabulary),
                    embedding_size=self.embedding_dim,
                    filter_sizes=map(int, self.filter_sizes.split(",")),
                    num_filters=self.num_filters,
                    l2_reg_lambda=self.l2_reg_lambda)
                # Define Training procedure
                global_step = tf.Variable(0, name="global_step", trainable=False)
                optimizer = tf.train.AdamOptimizer(self.learning_rate)
                grads_and_vars = optimizer.compute_gradients(cnn.loss)
                train_op = optimizer.apply_gradients(grads_and_vars, global_step=global_step)

                # Keep track of gradient values and sparsity (optional)
                grad_summaries = []
                for g, v in grads_and_vars:
                    if g is not None:
                        grad_hist_summary = tf.histogram_summary("{}/grad/hist".format(v.name), g)
                        sparsity_summary = tf.scalar_summary("{}/grad/sparsity".format(v.name), tf.nn.zero_fraction(g))
                        grad_summaries.append(grad_hist_summary)
                        grad_summaries.append(sparsity_summary)
                grad_summaries_merged = tf.merge_summary(grad_summaries)

                # Output directory for models and summaries
                print("Writing to {}\n".format(self.out_dir))

                # Summaries for loss and accuracy
                loss_summary = tf.scalar_summary("loss", cnn.loss)
                acc_summary = tf.scalar_summary("accuracy", cnn.accuracy)

                # Train Summaries
                train_summary_op = tf.merge_summary([loss_summary, acc_summary, grad_summaries_merged])
                train_summary_dir = os.path.join(self.out_dir, "summaries", "train")

                if self.write_summary:
                    train_summary_writer = tf.train.SummaryWriter(train_summary_dir, sess.graph_def)

                # Dev summaries
                dev_summary_op = tf.merge_summary([loss_summary, acc_summary])
                dev_summary_dir = os.path.join(self.out_dir, "summaries", "dev")

                if self.write_summary:
                    dev_summary_writer = tf.train.SummaryWriter(dev_summary_dir, sess.graph_def)
                else:
                    dev_summary_writer = None

                # Checkpoint directory. Tensorflow assumes this directory already exists so we need to create it
                checkpoint_dir = os.path.join(self.out_dir, "checkpoints")
                checkpoint_prefix = os.path.join(checkpoint_dir, "model")
                if not os.path.exists(checkpoint_dir):
                    os.makedirs(checkpoint_dir)
                saver = tf.train.Saver(tf.all_variables(), max_to_keep=1)

                # Initialize all variables
                sess.run(tf.initialize_all_variables())

                def train_step(x_batch, y_batch):
                    """
                    A single training step
                    """
                    feed_dict = {
                      cnn.input_x: x_batch,
                      cnn.input_y: y_batch,
                      cnn.dropout_keep_prob: self.dropout_keep_prob
                    }
                    _, step, summaries, loss, accuracy = sess.run(
                        [train_op, global_step, train_summary_op, cnn.loss, cnn.accuracy],
                        feed_dict)
                    time_str = datetime.datetime.now().isoformat()
                    print("{}: step {}, loss {:g}, acc {:g}".format(time_str, step, loss, accuracy))
                    if self.write_summary:
                        train_summary_writer.add_summary(summaries, step)

                def dev_step(x_batch, y_batch, writer=None):
                    """
                    Evaluates model on a dev set
                    """
                    feed_dict = {
                      cnn.input_x: x_batch,
                      cnn.input_y: y_batch,
                      cnn.dropout_keep_prob: 1.0
                    }
                    step, summaries, loss, accuracy = sess.run(
                        [global_step, dev_summary_op, cnn.loss, cnn.accuracy],
                        feed_dict)
                    time_str = datetime.datetime.now().isoformat()
                    print("{}: step {}, loss {:g}, acc {:g}".format(time_str, step, loss, accuracy))
                    if writer:
                        if self.write_summary:
                            writer.add_summary(summaries, step)

                # Generate batches

                if self.batch_size < 0:
                    if x_train.shape[0] % (-1 * self.batch_size) == 0:
                        force_batch_size = int(x_train.shape[0] / (-1 * self.batch_size))
                    else:
                        force_batch_size = int(x_train.shape[0] / (-1 * self.batch_size)) + 1
                    batches = data_helpers.batch_iter(
                        zip(x_train, y_train), force_batch_size, self.num_epochs)
                else:
                    batches = data_helpers.batch_iter(
                        zip(x_train, y_train), self.batch_size, self.num_epochs)
                # Training loop. For each batch...
                if self.continue_train:
                    try:
                        ckpt = tf.train.get_checkpoint_state(self.ckpt_dir)
                        if ckpt and ckpt.model_checkpoint_path:
                            saver.restore(sess, ckpt.model_checkpoint_path)
                    except:
                        pass
                for batch in batches:
                    x_batch, y_batch = zip(*batch)
                    train_step(x_batch, y_batch)
                    current_step = tf.train.global_step(sess, global_step)
                    if current_step % self.evaluate_every == 0:
                        print("\nEvaluation:")
                        dev_step(x_dev, y_dev, writer=dev_summary_writer)
                        print("")
                    if current_step % self.checkpoint_every == 0:
                        path = saver.save(sess, checkpoint_prefix, global_step=current_step)
                        print("Saved model checkpoint to {}\n".format(path))
        return True
