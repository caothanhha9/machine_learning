import re

from utils.constants import FilePath, Config, Constants, Messages, StopWords
from utils import data_helpers
import datetime
from utils import logger_gen
from core_node.train import Train
from core_node.predict import Predict
import numpy as np
import os


logger = logger_gen.initiate_logger('__name__')


def train_one(node_id,
              root_address=FilePath.TRAIN_ROOT_ADDRESS,
              vocabulary_path=FilePath.VOCABULARY_PATH,
              set_flag=False, continue_train=False,
              keep_balance=False):
    att_path = root_address[2] + str(node_id)
    val_path = root_address[3] + str(node_id)
    vocabulary_save_path = vocabulary_path + str(node_id)
    time_str = datetime.datetime.now().isoformat()
    # out_dir = os.path.join(os.path.curdir, FilePath.CHECK_POINT_PATH, str(node_id))
    out_dir = os.path.join(FilePath.CHECK_POINT_PATH, str(node_id))
    is_success = False

    if os.path.isfile(att_path) & os.path.isfile(val_path):
        train = Train(att_path, val_path, vocabulary_save_path, out_dir,
                      set_flag=set_flag, split_index=Config.TRAIN_SPLIT_INDEX,
                      learning_rate=Config.TRAIN_LEARNING_RATE,
                      filter_sizes=Config.FILTER_SIZES,
                      dropout_keep_prob=Config.TRAIN_DROPOUT_KEEP_PROB,
                      batch_size=Config.TRAIN_BATCH_SIZE,
                      num_epochs=Config.TRAIN_NUM_EPOCHS,
                      evaluate_every=Config.TRAIN_EVALUATE_EVERY,
                      continue_train=continue_train,
                      node_id=node_id,
                      keep_balance=keep_balance)
        train.run()
        is_success = True
    else:
        train_error_path = open(FilePath.TRAIN_ERROR_PATH, 'a')
        train_error_path.write(time_str + ' node id ' + str(node_id) + ': can NOT load train data\n')
        train_error_path.close()
    return is_success


def predict_all(sentence_list, setflag=False,
                node_list_path=FilePath.NODE_LIST_PATH,
                ckpt_path=FilePath.CHECK_POINT_PATH,
                root_address=FilePath.TRAIN_ROOT_ADDRESS,
                vocabulary_path=FilePath.VOCABULARY_PATH,
                root_project_path=FilePath.ROOT_PROJECT_PATH,
                feature_node_list_path=FilePath.FEATURE_NODE_LIST_PATH):

    # From node_id_map
    node_id_map = data_helpers.get_node_id_map(feature_node_list_path)
    node_id_list = range(len(node_id_map))
    final_feature_list = predict_all_by_list(sentence_list, node_id_list, node_list_path=node_list_path,
                                             feature_node_list_path=feature_node_list_path)
    return final_feature_list


def predict_all_by_list(sentence_list, node_id_list, setflag=False,
                        node_list_path=FilePath.NODE_LIST_PATH,
                        ckpt_path=FilePath.CHECK_POINT_PATH,
                        root_address=FilePath.TRAIN_ROOT_ADDRESS,
                        vocabulary_path=FilePath.VOCABULARY_PATH,
                        root_project_path=FilePath.ROOT_PROJECT_PATH,
                        feature_node_list_path=FilePath.FEATURE_NODE_LIST_PATH):
    node_id_map = data_helpers.get_node_id_map(feature_node_list_path)

    skip_id_list = data_helpers.get_skip_id_list(node_id_map)
    node_id_list = list(set(node_id_list) - set(skip_id_list))
    total_feature_id_map = []
    final_feature_list = [[] for _ in sentence_list]
    for node_id in node_id_list:
        att_path = root_address[2] + str(node_id)
        val_path = root_address[3] + str(node_id)
        [negative_path, positive_path] = [val_path, att_path]
        vocabulary_save_path = vocabulary_path + str(node_id)
        # out_dir = os.path.join(os.path.curdir, ckpt_path, str(node_id))
        out_dir = ckpt_path + '/' + str(node_id)
        total_feature_list = []
        if os.path.isfile(positive_path) & os.path.isfile(vocabulary_save_path) \
                & os.path.isdir(out_dir):
            new_predict = Predict(positive_path, negative_path, vocabulary_save_path, out_dir,
                                  set_flag=setflag, split_index=0)
            total_result_list, total_prob_list, total_sent_arr = new_predict.predict_all(sentence_list)
            for result_id, result_list in enumerate(total_result_list):
                definition_list = [Constants.INVERSE_CHUNK_LABEL_ARR[result_] for result_ in result_list]
                sent_arr = total_sent_arr[result_id]
                prob_list = total_prob_list[result_id]
                fn_fv_arr = data_helpers.extract_feature(sent_arr, definition_list, prob_list)
                feature_list = []
                for fn_fv in fn_fv_arr:
                    feature_name = fn_fv[0]
                    feature_value = fn_fv[1]
                    if len(feature_name) > 0:
                        # if not (feature_name == node_id_map[node_id]):
                        #     feature_name = ''
                        #     feature_value = ''
                        fn_id, fn_in_map = data_helpers.check_if_string_in_list(
                            feature_name, node_id_map)
                        if (fn_id > -1) & (fn_in_map == node_id_map[node_id]):
                            feature_name = fn_in_map
                        else:
                            feature_name = ''
                            feature_value = ''
                    # if len(feature_name) > 0:
                    #     feature_name = node_id_map[node_id]
                    feature_list.append((feature_name, feature_value))
                total_feature_list.append(feature_list)
            total_feature_id_map.append((node_id, total_feature_list))
        else:
            logger.error(Messages.FILE_NOT_FOUND)
            logger.error(positive_path)
            logger.error(vocabulary_save_path)
            logger.error(out_dir)
    for node_id_feature_list in total_feature_id_map:
        feature_list_ = node_id_feature_list[1]
        for final_ft_id in range(len(final_feature_list)):
            final_feature_list[final_ft_id] += feature_list_[final_ft_id]

    return final_feature_list


def train_list(node_id_list,
               feature_node_list_path=FilePath.FEATURE_NODE_LIST_PATH):
    node_id_map = data_helpers.get_node_id_map(feature_node_list_path)
    skip_id_list = data_helpers.get_skip_id_list(node_id_map)
    node_id_list = list(set(node_id_list) - set(skip_id_list))
    for node_id in node_id_list:
        train_one(node_id)
    return True


def predict_one_by_one(sentence, node_id, setflag=False,
                       node_list_path=FilePath.NODE_LIST_PATH,
                       ckpt_path=FilePath.CHECK_POINT_PATH,
                       root_address=FilePath.TRAIN_ROOT_ADDRESS,
                       vocabulary_path=FilePath.VOCABULARY_PATH,
                       root_project_path=FilePath.ROOT_PROJECT_PATH):

    # From node_id_map

    definition_list = []
    total_prob_list = []
    sent_arr = []
#    for node_id in range(len(node_id_map)):
    att_path = root_address[2] + str(node_id)
    val_path = root_address[3] + str(node_id)
    [negative_path, positive_path] = [val_path, att_path]
    vocabulary_save_path = vocabulary_path + str(node_id)
    # out_dir = os.path.join(os.path.curdir, ckpt_path, str(node_id))
    out_dir = ckpt_path + '/' + str(node_id)
    if os.path.isfile(positive_path) & os.path.isfile(vocabulary_save_path) \
            & os.path.isdir(out_dir):
        new_predict = Predict(positive_path, negative_path, vocabulary_save_path, out_dir,
                              set_flag=setflag, split_index=0)
        result_list, prob_list, sent_arr = new_predict.predict(sentence)
        definition_list = [Constants.INVERSE_CHUNK_LABEL_ARR[result_] for result_ in result_list]
        total_prob_list = prob_list
    else:
        logger.error(Messages.FILE_NOT_FOUND)
    return definition_list, total_prob_list, sent_arr


def predict_one_by_list(sentence, node_id_list,
                        feature_node_list_path=FilePath.FEATURE_NODE_LIST_PATH):
    total_feature_list = []
    node_id_map = data_helpers.get_node_id_map(feature_node_list_path)
    for node_id in node_id_list:
        definition_list, prob_list, sent_arr = predict_one_by_one(sentence, node_id)
        fn_fv_arr = data_helpers.extract_feature(sent_arr, definition_list, prob_list)
        for fn_fv in fn_fv_arr:
            feature_name = fn_fv[0]
            feature_value = fn_fv[1]
            if len(feature_name) > 0:
                # if not (feature_name == node_id_map[node_id]):
                #     feature_name = ''
                #     feature_value = ''
                fn_id, fn_in_map = data_helpers.check_if_string_in_list(
                    feature_name, node_id_map)
                if fn_id > -1:
                    feature_name = fn_in_map
                else:
                    feature_name = ''
                    feature_value = ''
            # if len(feature_name) > 0:
            #     feature_name = node_id_map[node_id]
            total_feature_list.append((feature_name, feature_value))
    return total_feature_list


def extract_feature_from_desc(desc_list):
    zip_desc_list = []
    parsed_feature_list = [[] for _ in desc_list]
    for desc_id, desc_ in enumerate(desc_list):
        if desc_ is not None:
            desc_arr_origin = desc_.split('\n')
            desc_arr_origin = [desc__.strip() for desc__ in desc_arr_origin if len(desc__.strip()) > 0]
            desc_arr = []
            for desc_sents in desc_arr_origin:
                desc_arr += re.split(r'\.(?:\s|\t)', desc_sents)
            desc_arr = [desc__.strip() for desc__ in desc_arr if len(desc__.strip()) > 0]
            for desc__ in desc_arr:
                zip_desc_list.append([desc_id, desc__])
    mix_id_list = list(np.array(zip_desc_list)[:, 0])
    mix_desc_list = list(np.array(zip_desc_list)[:, 1])
    mix_extracted_features = predict_all(mix_desc_list)
    for ext_ft_id, ext_ft_ in enumerate(mix_extracted_features):
        origin_id = mix_id_list[ext_ft_id]
        parsed_feature_list[int(origin_id)] += ext_ft_
    return parsed_feature_list


def parse_feature_from_desc(desc_list):
    final_parse_feature_list = []
    # regex_base_parsed_feature_arr_list = regex_base_app.extract_feature_from_desc(desc_list)
    # parsed_feature_arr_list = regex_base_parsed_feature_arr_list
    parsed_feature_arr_list = extract_feature_from_desc(desc_list)
    for parsed_feature_arr in parsed_feature_arr_list:
        parsed_feature_str = ''
        for fn_fv in parsed_feature_arr:
            if (len(fn_fv[0]) > 0) & (len(fn_fv[1]) > 0):
                parsed_feature_str += fn_fv[0] + ' => ' + fn_fv[1] + '\n'
        final_parse_feature_list.append(parsed_feature_str)
    return final_parse_feature_list


def parse_feature_from_desc_list_limited(desc_list, product_names):
    final_parse_feature_list = []
    for desc_id, desc_ in enumerate(desc_list):
        product_name = product_names[desc_id]
        if (desc_ is None) | (len(product_name) == 0):
            final_parse_feature_list.append('')
        else:
            desc_arr_origin = desc_.split('\n')
            desc_arr_origin = [desc__.strip() for desc__ in desc_arr_origin if len(desc__.strip()) > 0]
            desc_arr = []
            for desc_sents in desc_arr_origin:
                desc_arr += re.split(r'\.(?:\s|\t)', desc_sents)
            desc_arr = [desc__.strip() for desc__ in desc_arr if len(desc__.strip()) > 0]
            total_parsed_feature_arr = extract_feature_from_desc_limited(desc_arr, product_name)
            parsed_feature_str = ''
            for parsed_feature_arr in total_parsed_feature_arr:
                for fn_fv in parsed_feature_arr:
                    if (len(fn_fv[0]) > 0) & (len(fn_fv[1]) > 0):
                        parsed_feature_str += fn_fv[0] + ' => ' + fn_fv[1] + '\n'
            final_parse_feature_list.append(parsed_feature_str)
    return final_parse_feature_list


def extract_feature_from_desc_limited(desc_arr, product_name,
                                      feature_node_list_path=FilePath.FEATURE_NODE_LIST_PATH):
    try:
        node_id_map = data_helpers.get_node_id_map(feature_node_list_path)
        feature_nodes = data_helpers.get_node_attributes(product_name)
        feature_nodes = [data_helpers.str_encode(feature_)
                         for feature_ in feature_nodes]
        node_id_list = []
        for feature_ in feature_nodes:
            feature_id, feature_in_map = data_helpers.check_if_string_in_list(feature_, node_id_map)
            if feature_id > -1:
                node_id_list.append(feature_id)
        final_feature_list = predict_all_by_list(desc_arr, node_id_list,
                                                 feature_node_list_path=feature_node_list_path)
    except Exception as inst:
        print(inst)
        final_feature_list = []
    return final_feature_list
