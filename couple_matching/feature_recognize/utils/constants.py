# -*- coding: utf-8 -*-
import os


class Messages(object):
    FILE_NOT_FOUND = 'File NOT FOUND '
    CANNOT_WRITE_FILE = 'CAN NOT write file '
    AUTO_EXCLUDE_LEARNING = 'Auto (exclude) learning...'
    CANNOT_FIND_NODE = 'Can not find this node in relationship tree. '
    CHECKPOINT_NOT_FOUND = 'No checkpoint file found. '
    NEW_URL_FOUND = 'We found new url in train table'
    UNABLE_TO_START_THREAD = 'Error: unable to start thread'


class FilePath(object):
    # ROOT_PROJECT_PATH = '.'
    ROOT_PROJECT_PATH = '/home/hact/workspace/python/machinelearning/couple_matching/feature_recognize'
    ROOT_MODEL_PATH = '/storage/hact/workspace/python/dl-cnn-product-v06/feature_recognize'
    LOG_FILE_PATH = ROOT_PROJECT_PATH + '/log/log_detail'
    TRAIN_ROOT_ADDRESS = [ROOT_PROJECT_PATH + '/data/train/train_data_anti',
                          ROOT_PROJECT_PATH + '/data/train/train_data_sim',
                          ROOT_PROJECT_PATH + '/data/train/train_data_att',
                          ROOT_PROJECT_PATH + '/data/train/train_data_val']
    DB_TRAIN_ROOT_ADDRESS = [ROOT_PROJECT_PATH + '/data/db_train/train_data_anti',
                             ROOT_PROJECT_PATH + '/data/db_train/train_data_sim']
    NODE_LIST_PATH = ROOT_PROJECT_PATH + '/node_list'
    LEARNT_NODE_ID_PATH = ROOT_PROJECT_PATH + '/update/learnt_node_list'
    LEARNT_NODE_NAME_PATH = ROOT_PROJECT_PATH + '/update/learnt_nodename_list'
    DATA_TRAIN_DIR = ROOT_PROJECT_PATH + '/data/train'
    DATA_MEMORY_NODES = ROOT_PROJECT_PATH + '/data/memory/nodes'
    DATA_LABEL_PATH = ROOT_PROJECT_PATH + '/data/label/train_data'
    NODE_RELATIONSHIP_PATH = ROOT_PROJECT_PATH + '/data/memory/node_relationship'
    NODE_TREE_PATH = ROOT_PROJECT_PATH + '/data/train/node_relationship.xml'
    VOCABULARY_PATH = ROOT_PROJECT_PATH + '/vocabulary/vocabulary.pkl'
    CHECK_POINT_PATH = ROOT_MODEL_PATH + '/runs'
    TRAIN_ERROR_PATH = ROOT_PROJECT_PATH + '/log/train_error_message'
    WARN_GRAPH_NODE_PATH = ROOT_PROJECT_PATH + '/warn_graph_node_path'
    PREDICT_PATH = ROOT_PROJECT_PATH + '/data/predict/data'
    RESULT_PATH = ROOT_PROJECT_PATH + '/data/predict/data_result'
    PARSED_DB_DATA_PATH = ROOT_PROJECT_PATH + '/data/predict/db_data_result'
    TEST_OUTPUT_TEMPLATE_PATH = ROOT_PROJECT_PATH + '/web_server/tmpl/test.html'
    FIX_TITLE_PN_PATH = ROOT_PROJECT_PATH + '/data/memory/fix_node_data'
    DEBUG_FILE_PATH = ROOT_PROJECT_PATH + '/debug.txt'
    # Sim file paths:
    # SIM_DATA_BASE_PATH = NODE_LIST_PATH
    SIM_DATA_BASE_PATH = ROOT_PROJECT_PATH + '/data/cnn_sim/pn_node_list'  # '/data/cnn_sim/base_map_data'
    SIM_DATA_TRAIN_PATH = ROOT_PROJECT_PATH + '/data/cnn_sim/base_data'  # '/data/sim/data_process.txt'
    SIM_MODEL_PATH = ROOT_PROJECT_PATH + '/model/doc2vec.model'
    SIM_DATA_PATH = ROOT_PROJECT_PATH + '/data/predict/data'
    SIM_DOC_FILE_LIST = [SIM_DATA_TRAIN_PATH, SIM_DATA_BASE_PATH]
    # CNN Sim file paths:
    FEATURE_NODE_LIST_PATH = ROOT_PROJECT_PATH + '/data/feature_node_list'
    STOP_WORD_PATH = ROOT_PROJECT_PATH + '/data/stop_words'
    SKIP_FEATURES_PATH = ROOT_PROJECT_PATH + '/data/skip_features'
    FEATURE_ATTRIBUTE_LIST_PATH = ROOT_PROJECT_PATH + '/data/feature_attribute_list'
    # Regex file paths:
    REGEX_TRAIN_DATA_PATH = ROOT_PROJECT_PATH + '/regex_base/regex_train_data'


class Constants(object):
    TOP_NODE = ['categories']
    TEXT = 'text'
    XML = 'xml'
    PAD_WORD = '<PAD/>'
    CHUNK_LABEL_DICT = {'O': [1, 0, 0, 0, 0], 'B-N': [0, 1, 0, 0, 0], 'I-N': [0, 0, 1, 0, 0],
                        'B-V': [0, 0, 0, 1, 0], 'I-V': [0, 0, 0, 0, 1]}
    INVERSE_CHUNK_LABEL_ARR = ['O', 'B-N', 'I-N', 'B-V', 'I-V']
    LOAD_TIME = 0.135
    CALC_TIME = 0.0058
    DB_PRODUCT_NAME = 0
    DB_TITLE = 1
    DB_CAT_NAME = 2
    DB_DETAILS = 3
    DB_URL = 4
    TEST_DB_ID = 0
    TEST_DB_TITLE = 1
    DB_CAT_NAME_DELIM = '>'
    DB_PRO_NAME_DELIM = ','
    ENCODE_TYPE = 'utf-8'
    DECODE_TYPE = 'utf8'
    GENERAL_NODE = u'general'
    GENERAL_PREFIX_ARR = ['mộtxxx chiếcxxx', 'haixxx cáixxx', 'nhữngxxx']
    GENERAL_SUFFIX_ARR = ['caoxxx cấpxxx', 'hàngxxx độcxxx', 'lạxxx']
    ANTI_PATTERN_ARR = ['cao cấp', 'nhập khẩu', 'tặng', 'kèm', 'tặng kèm']
    NO_MEANING_PATTERNS = [r'(?:\s|^)(\w*\d+\w*)(?:\s|$)',
                           r'(?:\s|^)(\w)(?:\s|$)',
                           r'(?:\s|^)([0-9]+)(?:\s|$)',
                           r'(?:\s|^)bộ(?:\s|$)',
                           r'(?:\s|^)sản\s+phẩm(?:\s|$)',
                           r'(?:\s|^)combo(?:\s|$)',
                           r'(?:\s|^)cao\s+cấp(?:\s|$)',
                           r'(?:\s|^)nhập\s+khẩu(?:\s|$)',
                           r'(?:\s|^)tặng(?:\s|$)',
                           r'(?:\s|^)kèm(?:\s|$)',
                           r'(?:\s|^)tặng\s+kèm(?:\s|$)',
                           ]


class Config(object):
    NODE_RELATIONSHIP_DATA_TYPE = Constants.XML
    MYSQL_HOST = '192.168.23.122'
    MYSQL_USER = 'root'
    MYSQL_PASSWORD = 'iloveyou'
    DB_NAME = 'crawldb'
    PREDICT_PROB_LIMIT = 0.55
    INDIVIDUAL_PREDICT_PROB_LIMIT = 0.8
    FINAL_PREDICT_PROB_LIMIT = 0.75
    TRAIN_DROPOUT_KEEP_PROB = 1.0  # Params: dropout 0.65, l2_lambda 0.035, train_data length: sim 8, anti 8
    TRAIN_L2_REG_LAMBDA = 0.0  # Params: dropout 1.0, l2_lambda 0.03, train_data length: sim 8, anti 8
    # Params: dropout 0.8, l2_lambda 0.005, train_data length: sim 8, anti 8
    TRAIN_LEARNING_RATE = 1e-4
    TRAIN_SPLIT_INDEX = 0
    PROB_THRESHOLD_DICT = {'O': 0.5, 'B-N': 0.8, 'I-N': 0.6,
                           'B-V': 0.7, 'I-V': 0.5}
    INBV_DISTANCE_RANGE = range(0, 3)
    FILTER_SIZES = "6,7"
    TRAIN_BATCH_SIZE = 1000
    TRAIN_NUM_EPOCHS = 500
    TRAIN_EVALUATE_EVERY = 1000
    SET_MAX_PREDICT = False
    SET_MAX_FINAL_PREDICT = False
    SET_ANTI_LIMIT = True
    TRAIN_DATA_ANTI_LIMIT = 1500
    TRAIN_PN_DATA_ANTI_MIN = 25
    TRAIN_DATA_ANTI_MIN = 15
    TRAIN_DATA_SIM_MIN = 15
    SET_CHECK_ANTI_MIN = True
    SET_CHECK_SIM_MIN = True
    SET_EXCLUDE_LEARN = False
    SET_TITLE_TO_ANTI = False
    SET_FINAL_PREDICT_PROB = True
    SELF_LEARN_NODE_NAME = True
    LEARN_DESCRIPTION = False
    SET_PRIORITY = 'cnn'
    SET_ANTI_INTERFERE = True
    # Sim config
    SIM_STD_CRITERIA = 0.9999
    SIM_STD_THRESHOLD = 0.9
    SIM_NUM_EPOCH = 1000
    # CNN Sim config
    SET_CNN_SIM_ANTI_LIMIT = True
    CNN_SIM_FILTER_SIZES = "2,3,4,5"
    CNN_SIM_ANTI_LIMIT = 3
    CNN_SIM_SIM_LIMIT = 3
    CNN_SIM_PREDICT_PROB_LIMIT = 0.8
    CNN_SIM_SET_FINAL_MAX_PREDICT = True
    CNN_SIM_TRAIN_DROPOUT_KEEP_PROB = 0.8  # 0.65
    CNN_SIM_TRAIN_L2_REG_LAMBDA = 0.005  # 0.035
    CNN_SIM_TRAIN_LEARNING_RATE = 1e-3
    CNN_SIM_HIDDEN_DIM = 100
    CNN_SIM_TRAIN_SPLIT_INDEX = 0
    CNN_SIM_TRAIN_BATCH_SIZE = 10000
    CNN_SIM_TRAIN_NUM_EPOCHS = 4000
    CNN_SIM_TRAIN_EVALUATE_EVERY = 10000


class UrlMap(object):
    TIKI = 0
    ENBAC = 1
    NEMO = 2
    LAZADA = 3
    PLAZA = 4


class StopWords:
    class __StopWords:
        def __init__(self, arg):
            stop_words = []
            if os.path.isfile(arg):
                try:
                    stop_words = list(open(arg).readlines())
                    stop_words = [st_wd.strip() for st_wd in stop_words if len(st_wd.strip()) > 0]
                except:
                    pass
            self.val = stop_words

        def __str__(self):
            return repr(self) + str(self.val)
    instance = None

    def __init__(self, arg):
        if not StopWords.instance:
            StopWords.instance = StopWords.__StopWords(arg)
        else:
            stop_words = []
            if os.path.isfile(arg):
                try:
                    stop_words = list(open(arg).readlines())
                    stop_words = [st_wd.strip() for st_wd in stop_words if len(st_wd.strip()) > 0]
                except:
                    pass
            StopWords.instance.val = stop_words

    def __getattr__(self, name):
        return getattr(self.instance, name)