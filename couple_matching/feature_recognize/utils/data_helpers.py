# -*- coding: utf-8 -*-
import os
import unicodedata

from feature_recognize.utils.constants import FilePath, Constants, Config, StopWords
from product_suggest.utils import neo4j_connector
import re
import numpy as np
from collections import Counter
from product_recognize.utils import shelve_connector


def str_encode(string):
    try:
        string = unicodedata.normalize('NFC', string)
    except:

        pass
    try:
        string = string.encode(encoding=Constants.ENCODE_TYPE)
    except:
        pass
    return string


def str_decode(string):
    try:
        string = string.decode(encoding=Constants.ENCODE_TYPE)
        string = unicodedata.normalize('NFC', string)
    except:
        pass
    return string


def clean_str(string):
    string = str_decode(string).lower()
    string = str_encode(string)
    string = re.sub(r"\(([0-9a-zA-Z\s\.,]+)\)", " ", string)
    string = re.sub(r"-", " - ", string)
    string = re.sub(r"–", " - ", string)
    # string = re.sub(r"\xe2\x80\x93", " ", string)
    string = re.sub(r",(?!\d)", " , ", string)
    # string = re.sub(r'"', ' " ', string)
    string = re.sub(r'"', ' " ', string)
    string = re.sub(r"''", ' " ', string)
    string = re.sub(r'\.(?!\d)', ' . ', string)
    string = re.sub(r"!", " ! ", string)
    string = re.sub(r"/", " / ", string)
    string = re.sub(r";", " ; ", string)
    string = re.sub(r"&", " & ", string)
    # string = re.sub(r":", " : ", string)
    string = re.sub(r":", " ", string)
    string = re.sub(r"\+", " + ", string)
    string = re.sub(r"\(", " ( ", string)
    string = re.sub(r"\)", " ) ", string)
    string = re.sub(r"\?", " ? ", string)
    string_arr = re.split('([.\d]+)', string.strip())
    string = ' '.join(string_arr)
    string = re.sub('\s+', ' ', string)
    return string


def join_encoded_string_array(encoded_string_array, delimiter=' '):
    decoded_string_arr = [str_decode(word_) for word_ in encoded_string_array]
    joined_encoded_string = str_encode(delimiter.join(decoded_string_arr))
    return joined_encoded_string


def make_abstract_str(string):
    string = re.sub(r'\d+(?:[,.]\d+)*', 'numberplaceholder', string)
    return string


def make_abstract_array_from_str(string):
    # match = re.findall(r'([0-9,\.]+)', string)
    # abstract_string = re.sub(r'([0-9,\.]+)', 'numberplaceholder', string)
    match = re.findall(r'\d+(?:[,.]\d+)*', string)
    abstract_string = re.sub(r'\d+(?:[,.]\d+)*', 'numberplaceholder', string)
    abstract_string_arr = abstract_string.split()
    real_string_arr = []
    pattern_index = -1
    for abstract_word in abstract_string_arr:
        match_reverse = re.findall('numberplaceholder', abstract_word)
        if len(match_reverse) > 0:
            for _ in match_reverse:
                pattern_index += 1
                instance_value = match[pattern_index]
                real_string_arr.append(instance_value)
        else:
            real_string_arr.append(abstract_word)
    return abstract_string_arr, real_string_arr


def slide_str_and_pad(string_arr, slide_size):
    padded_string = slide_size * (Constants.PAD_WORD + ' ')
    padded_string += ' '.join(string_arr)
    padded_string += (' ' + Constants.PAD_WORD) * slide_size
    padded_string_arr = padded_string.split()
    slided_arr_list = []
    for id_, word_ in enumerate(string_arr):
        slided_arr = padded_string_arr[id_:id_ + 2 * slide_size + 1]
        slided_arr_list.append(slided_arr)
    return slided_arr_list


def str_strip(string):
    string = str_decode(string).strip()
    string = str_encode(string).strip()
    return string


def replace_symbol_by_space(string):
    string = re.sub(r"-", " ", string)
    string = re.sub(r"–", " ", string)
    string = re.sub(r",", " ", string)
    string = re.sub(r"!", " ", string)
    string = re.sub(r"/", " ", string)
    string = re.sub(r"&", " ", string)
    string = re.sub(r":", " ", string)
    string = re.sub("\s+", " ", string)
    string = str_strip(string)
    return string


def remove_symbol(string):
    string = replace_symbol_by_space(string)
    string = re.sub("\s+", "", string)
    string = str_strip(string)
    return string


def check_if_string_in_list(string, string_array):
    processed_string = remove_symbol(string)
    processed_string_array = [remove_symbol(sent_) for sent_ in string_array]
    string_index = -1
    string_in_array = ''

    if processed_string in processed_string_array:
        string_index = processed_string_array.index(processed_string)
        string_in_array = string_array[string_index]
    return string_index, string_in_array


def get_node_id_map(node_list_path):
    node_id_map = list(open(node_list_path).readlines())
    node_id_map = [s.strip().lower() for s in node_id_map]
    node_id_map = [str_decode(node_name) for node_name in node_id_map]
    node_id_map = [str_encode(node_name) for node_name in node_id_map]
    return node_id_map


def check_two_pn_interfere(pn1, pn2):
    w1_in_pn2 = 0
    w2_in_pn1 = 0
    pn1_arr = pn1.split()
    pn2_arr = pn2.split()
    for ea_w in pn1_arr:
        if ea_w in pn2_arr:
            w1_in_pn2 += 1
    for ea_w in pn2_arr:
        if ea_w in pn1_arr:
            w2_in_pn1 += 1

    if w1_in_pn2 == len(pn1_arr):
        if w2_in_pn1 == len(pn2_arr):
            is_interfere = 3
        else:
            is_interfere = 1
    elif w1_in_pn2 > 0:
        if w2_in_pn1 == len(pn2_arr):
            is_interfere = 2
        else:
            is_interfere = 0
    else:
        is_interfere = -1
    return is_interfere


def search_correct_spell_str_in_array(product_name, origin_product_names):
    product_names = []
    same_product_names = []
    parent_product_names = []
    children_product_names = []
    other_product_names = []
    origin_product_names = [origin_pn_.strip() for origin_pn_ in origin_product_names]
    if (len(product_name) > 0) & (len(origin_product_names) > 0):
        remained_product_names = []
        processed_product_name = replace_symbol_by_space(product_name)
        for origin_pn_ in origin_product_names:
            # origin_pn_ = origin_pn_.strip()
            processed_origin_pn_ = replace_symbol_by_space(origin_pn_)
            is_interfere = check_two_pn_interfere(processed_product_name, processed_origin_pn_)
            if is_interfere == 3:
                same_product_names.append(origin_pn_)
            elif is_interfere == 1:
                parent_product_names.append(origin_pn_)
            elif is_interfere == 2:
                children_product_names.append(origin_pn_)
            elif is_interfere == 0:
                other_product_names.append(origin_pn_)
            else:
                remained_product_names.append(origin_pn_)
        product_names += same_product_names
        product_names += parent_product_names
        product_names += children_product_names
        product_names += other_product_names
    return product_names


def load_data_and_labels(att_path, val_path, slide_size=3):
    if os.path.isfile(att_path):
        att_examples = list(open(att_path).readlines())
        att_examples = [s.strip() for s in att_examples if len(s.strip()) > 0]
    else:
        att_examples = []
    if os.path.isfile(val_path):
        val_examples = list(open(val_path).readlines())
        val_examples = [s.strip() for s in val_examples if len(s.strip()) > 0]
    else:
        val_examples = []
    # Split by words
    att_examples = [clean_str(sent) for sent in att_examples]
    att_examples = [sent for sent in att_examples if len(sent) > 0]
    val_examples = [clean_str(sent) for sent in val_examples]
    val_examples = [sent for sent in val_examples if len(sent) > 0]
    print(att_examples)
    print(val_examples)
    chunk_label_dict = Constants.CHUNK_LABEL_DICT

    x_text = []
    words = []
    labels = []
    for att_ex_ in att_examples:
        # sent_arr = att_ex_.split()
        sent_arr, _ = make_abstract_array_from_str(att_ex_)
        words += sent_arr
        for val_ex_ in val_examples:
            # val_ex_arr = val_ex_.split()
            val_ex_arr, _ = make_abstract_array_from_str(val_ex_)
            full_sent_arr = sent_arr + val_ex_arr
            slided_sent_arr = slide_str_and_pad(full_sent_arr, slide_size)
            for word_id, word_ in enumerate(sent_arr):
                x_text.append(slided_sent_arr[word_id])
                if word_id == 0:
                    labels.append(chunk_label_dict['B-N'])
                else:
                    labels.append(chunk_label_dict['I-N'])

    for val_ex_ in val_examples:
        # sent_arr = val_ex_.split()
        sent_arr, _ = make_abstract_array_from_str(val_ex_)
        words += sent_arr
        for att_ex_ in att_examples:
            # att_ex_arr = att_ex_.split()
            att_ex_arr, _ = make_abstract_array_from_str(att_ex_)
            full_sent_arr = att_ex_arr + sent_arr
            slided_sent_arr = slide_str_and_pad(full_sent_arr, slide_size)
            for word_id, word_ in enumerate(sent_arr):
                x_text.append(slided_sent_arr[word_id + len(att_ex_arr)])
                if word_id == 0:
                    labels.append(chunk_label_dict['B-V'])
                else:
                    labels.append(chunk_label_dict['I-V'])
    # words.append(':')
    # for att_ex_ in att_examples:
    #     att_ex_arr = att_ex_.split()
    #     for val_ex_ in val_examples:
    #         val_ex_arr = val_ex_.split()
    #         full_sent_arr = att_ex_arr + [':'] + val_ex_arr
    #         slided_sent_arr = slide_str_and_pad(full_sent_arr, slide_size)
    #         x_text.append(slided_sent_arr[len(att_ex_arr)])
    #         labels.append(chunk_label_dict['O'])
    zero_padded_arr = [Constants.PAD_WORD]
    slided_zero_padded_arr = slide_str_and_pad(zero_padded_arr, slide_size)
    x_text.append(slided_zero_padded_arr[0])
    labels.append(chunk_label_dict['O'])

    return [x_text, labels, words]


def build_vocab(words):
    """
    :param words
    Builds a vocabulary mapping from word to index based on the sentences.
    Returns vocabulary mapping and inverse vocabulary mapping.
    """
    # Build vocabulary
    vocabulary_size = len(words)
    count = [[Constants.PAD_WORD, -1]]
    count.extend(Counter(words).most_common(vocabulary_size - 1))
    dictionary = dict()
    for word, _ in count:
        dictionary[word] = len(dictionary)
    data = list()
    unk_count = 0
    for word in words:
        if word in dictionary:
            index = dictionary[word]
        else:
            index = 0  # dictionary['UNK']
            unk_count += 1
        data.append(index)
    count[0][1] = unk_count
    reverse_dictionary = dict(zip(dictionary.values(), dictionary.keys()))
    return dictionary, reverse_dictionary
    # word_counts = Counter(words)
    # # word_counts.update({'UNK': 0})
    # # Mapping from index to word
    # vocabulary_inv = [x[0] for x in word_counts.most_common()]
    # # Mapping from word to index
    # vocabulary = {x: i for i, x in enumerate(vocabulary_inv)}
    # return [vocabulary, vocabulary_inv]


def build_input_data(sentences, labels, vocabulary):
    """
    :param sentences
    :param labels
    :param vocabulary
    Maps sentencs and labels to vectors based on a vocabulary.
    """
    # Replace unknown word with 'UNK' or '<PAD/>'
    x = np.array([[vocabulary[word] if word in vocabulary else vocabulary[Constants.PAD_WORD] for word in sentence]
                  for sentence in sentences])
    y = np.array(labels)
    return [x, y]


def load_data(att_path, val_path):
    sentences, labels, words = load_data_and_labels(att_path, val_path)
    vocabulary, vocabulary_inv = build_vocab(words)
    x, y = build_input_data(sentences, labels, vocabulary)
    with open(FilePath.DEBUG_FILE_PATH, "a") as text_file:
        for sentence_id, sentence_ in enumerate(sentences):
            text_file.write("sentence: %s\n" % sentence_)
            text_file.write("label: %s\n" % y[sentence_id])
    """
    zero_anti_sentence = pad_sentences([[Constants.PAD_WORD]])
    zero_anti_label = [[1, 0]]
    zero_x, zero_y = build_input_data(zero_anti_sentence, zero_anti_label, vocabulary)
    np.append(x, zero_x)
    np.append(y, zero_y)
    """
    return [x, y, vocabulary, vocabulary_inv]


def make_data_from_string(sentence, vocabulary, is_a_list=False, config_path='',
                          is_fix_sequence_length=False, slide_size=3):
    sent = clean_str(sentence).strip()
    sent_arr, origin_sent_arr = make_abstract_array_from_str(sent)
    slided_sentences = slide_str_and_pad(sent_arr, slide_size)

    # Replace unknown word with 'UNK' or '<PAD/>'
    x = np.array([[vocabulary[word] if word in vocabulary else vocabulary[Constants.PAD_WORD]
                  for word in sentence] for sentence in slided_sentences])
    # x = np.array([[vocabulary[word] if word in vocabulary else 0 for word in sentence]
    #               for sentence in padded_sentences])
    return x, origin_sent_arr


def make_data_from_string_array(sentence_list, vocabulary, is_a_list=False, config_path='',
                                is_fix_sequence_length=False, slide_size=3):
    x_test = []
    total_origin_sent_arr = []
    for sentence in sentence_list:
        x, origin_sent_arr = make_data_from_string(sentence, vocabulary)
        x_test.append(x)
        total_origin_sent_arr.append(origin_sent_arr)
    return x_test, total_origin_sent_arr


def batch_iter(data, batch_size, num_epochs):
    """
    :param data
    :param batch_size
    :param num_epochs
    Generates a batch iterator for a dataset.
    """
    data = np.array(data)
    data_size = len(data)
    if data_size % batch_size == 0:
        num_batches_per_epoch = data_size / batch_size
    else:
        num_batches_per_epoch = int(len(data)/batch_size) + 1
    for epoch in range(num_epochs):
        # Shuffle the data at each epoch
        shuffle_indices = np.random.permutation(np.arange(data_size))
        shuffled_data = data[shuffle_indices]
        for batch_num in range(num_batches_per_epoch):
            start_index = batch_num * batch_size
            end_index = min((batch_num + 1) * batch_size, data_size)
            yield shuffled_data[start_index:end_index]


def extract_feature(sent_arr, feature_arr, prob_list, prob_threshold_dict=Config.PROB_THRESHOLD_DICT):
    stop_words = StopWords(FilePath.STOP_WORD_PATH).val
    end_index = -1
    offset = 0
    total_fn_fv = []
    while end_index < (len(feature_arr) - 1):
        feature_arr_ = feature_arr[offset:len(feature_arr)]
        if len(feature_arr_) > 0:
            fn_fv = extract_fn(feature_arr_, prob_list, prob_threshold_dict)
            # if fn_fv['fv-stop'] == -1:
            #     fn_fv['fv-stop'] = len(sent_arr)
            if (fn_fv['fn-start'] > -1) & (fn_fv['fn-stop'] > -1) & (fn_fv['fv-start'] > -1) & (fn_fv['fv-stop'] > -1):
                feature_name_arr = sent_arr[offset + fn_fv['fn-start']:offset + fn_fv['fn-stop']]
                # feature_name_arr = [word_ for word_ in feature_name_arr if not (word_ in stop_words)]
                feature_name_arr = crop_by_stop_words(feature_name_arr, stop_words)
                feature_name = join_encoded_string_array(feature_name_arr)
                feature_value_arr = sent_arr[offset + fn_fv['fv-start']:offset + fn_fv['fv-stop']]
                # feature_value_arr = [word_ for word_ in feature_value_arr if not (word_ in stop_words)]
                feature_value_arr = crop_by_stop_words(feature_value_arr, stop_words)
                feature_value = join_encoded_string_array(feature_value_arr)
                feature_value_decode = str_decode(feature_value)
                feature_value_decode = re.sub(r'(\.|-)\s*$', '', feature_value_decode)
                feature_value_decode = feature_value_decode.strip()
                feature_value = str_encode(feature_value_decode)
                if len(feature_value) > 0:
                    total_fn_fv.append((feature_name, feature_value))
            offset = fn_fv['fv-stop']
            end_index += (abs(offset) + 1)
        else:
            break
    if len(total_fn_fv) > 0:
        print('*' * 50)
        print(sent_arr)
        print(feature_arr)
        print(prob_list)
        print(total_fn_fv)
        print('*' * 50)
    return total_fn_fv
    # feature_name = ''
    # feature_value = ''
    # fn_start_index = -1
    # fn_stop_index = -1
    # fv_start_index = -1
    # fv_stop_index = -1
    # fn_flags = [0, 0]
    # fv_flags = [0, 0]
    # for word_id, word_ in enumerate(sent_arr):
    #     if ((feature_arr[word_id] == 'B-N') & (prob_list[word_id] > prob_threshold_dict['B-N']) &
    #             (fn_flags[1] == 0)):
    #         fn_flags[0] = 1
    #         fn_start_index = word_id
    #     elif (fn_flags[0] == 1) & (fn_flags[1] == 0):
    #         if not ((feature_arr[word_id] == 'I-N') & (prob_list[word_id] > prob_threshold_dict['I-N'])):
    #             fn_stop_index = word_id
    #             fn_flags[1] = 1
    #     if (fn_flags[0] == 1) & (fn_flags[1] == 1):
    #         if ((feature_arr[word_id] == 'B-V') & (prob_list[word_id] > prob_threshold_dict['B-V']) &
    #                 (fv_flags[0] == 0)):
    #             fv_flags[0] = 1
    #             fv_start_index = word_id
    #         elif ((feature_arr[word_id] == 'I-V') & (prob_list[word_id] > prob_threshold_dict['I-V']) &
    #                 (fv_flags[0] == 0)):
    #             fv_flags[0] = 1
    #             fv_start_index = word_id
    #         elif (fv_flags[0] == 1) & (fv_flags[1] == 0):
    #             if not ((feature_arr[word_id] == 'I-V') & (prob_list[word_id] > prob_threshold_dict['I-V'])):
    #                 fv_stop_index = word_id
    #                 fv_flags[1] = 1
    #             else:
    #                 if word_id == (len(sent_arr) - 1):
    #                     fv_stop_index = word_id + 1
    #                     fv_flags[1] = 1

    # if fn_start_index > -1:
    #     if fn_stop_index > -1:
    #         feature_name_arr = sent_arr[fn_start_index:fn_stop_index]
    #     else:
    #         feature_name_arr = [sent_arr[fn_start_index]]
    #     feature_name_arr = [word_ for word_ in feature_name_arr if not (word_ in stop_words)]
    #     feature_name = ' '.join(feature_name_arr)
    #     if (fv_start_index > -1) & ((fv_start_index - fn_start_index) in Config.INBV_DISTANCE_RANGE):
    #         feature_value_arr = sent_arr[fv_start_index:fv_stop_index]
    #         feature_value_arr = [word_ for word_ in feature_value_arr if not (word_ in stop_words)]
    #         feature_value = ' '.join(feature_value_arr)
    # return feature_name, feature_value


def extract_fn(feature_arr, prob_list, prob_threshold_dict=Config.PROB_THRESHOLD_DICT):
    ft_dict = {'fn-start': -1, 'fn-stop': -1, 'fv-start': -1, 'fv-stop': -1}
    fn_flags = {'start': False, 'stop': False}
    fv_flags = {'start': False, 'stop': False}
    broken_down = False
    for ft_id, ft_ in enumerate(feature_arr):
        if (not fn_flags['start']) & (not fn_flags['stop']):
            if (ft_ == 'B-N') & (prob_list[ft_id] > prob_threshold_dict['B-N']):
                fn_flags['start'] = True
                ft_dict['fn-start'] = ft_id
        elif fn_flags['start'] & (not fn_flags['stop']):
            if (not (ft_ == 'I-N')) | (prob_list[ft_id] < prob_threshold_dict['I-N']):
                fn_flags['stop'] = True
                ft_dict['fn-stop'] = ft_id
            if (ft_ == 'I-N') & (prob_list[ft_id] < prob_threshold_dict['I-N']):
                broken_down = True
        if fn_flags['start'] & fn_flags['stop']:
            if (not fv_flags['start']) & (not fv_flags['stop']):
                if (((ft_ == 'B-V') & (prob_list[ft_id] > prob_threshold_dict['B-V'])) |
                        ((ft_ == 'I-V') & (prob_list[ft_id] > prob_threshold_dict['I-V']))):
                    fv_flags['start'] = True
                    ft_dict['fv-start'] = ft_id
                if (ft_ == 'I-V') & (prob_list[ft_id] < prob_threshold_dict['B-V']):
                    broken_down = True
            elif fv_flags['start'] & (not fv_flags['stop']):
                if (not (ft_ == 'I-V')) | (prob_list[ft_id] < prob_threshold_dict['I-V']):
                    fv_flags['stop'] = True
                    ft_dict['fv-stop'] = ft_id
                if ft_id == len(feature_arr) - 1:
                    fv_flags['stop'] = True
                    ft_dict['fv-stop'] = ft_id + 1
            if fv_flags['start'] & fv_flags['stop']:
                break
                # ft_dict['fn-start'] = -1
                # ft_dict['fn-stop'] = -1
                # ft_dict['fv-start'] = -1
                # ft_dict['fv-stop'] = -1
                # # fn_flags['start'] = False
                # fn_flags['stop'] = False
                # fv_flags['start'] = False
                # fv_flags['stop'] = False
    if ft_dict['fv-stop'] == -1:
        ft_dict['fv-stop'] = len(feature_arr)
    if not broken_down:
        return ft_dict
    else:
        return {'fn-start': -1, 'fn-stop': -1, 'fv-start': -1, 'fv-stop': -1}


def crop_by_stop_words(feature_arr, stop_words):
    crop_feature_arr = []
    for feature_ in feature_arr:
        if feature_ not in stop_words:
            crop_feature_arr.append(feature_)
        else:
            break
    return crop_feature_arr


def get_skip_id_list(node_id_map, skip_features_path=FilePath.SKIP_FEATURES_PATH):
    skip_features = StopWords(skip_features_path).val
    skip_id_list = []
    for skip_feature in skip_features:
        f_id, f_in_map = check_if_string_in_list(skip_feature, node_id_map)
        if f_id > -1:
            skip_id_list.append(f_id)
    return skip_id_list


def get_node_attributes(product_name, is_save=False,
                        feature_attribute_list_path=FilePath.FEATURE_ATTRIBUTE_LIST_PATH):
    new_shelve = shelve_connector.ShelveConnector(feature_attribute_list_path)
    is_exist = new_shelve.is_exist(product_name)
    if is_save | (not is_exist):
        attributes = neo4j_connector.get_node_attributes(product_name)
        new_shelve = shelve_connector.ShelveConnector(feature_attribute_list_path)
        new_shelve.save(product_name, attributes)
    attributes = new_shelve.get(product_name)
    if attributes is None:
        attributes = []
    return attributes


