import logging
import logging.handlers
from feature_recognize.utils.constants import FilePath


def initiate_logger(name_space='app'):
    logging.basicConfig(filename=FilePath.LOG_FILE_PATH, level=logging.ERROR,
                        maxBytes=1000, backupCount=2)
    logger = logging.getLogger(name_space)
    logger.setLevel(logging.DEBUG)
#    logger.propagate = False

    log_handler = logging.handlers.RotatingFileHandler(
        FilePath.LOG_FILE_PATH, maxBytes=1000, backupCount=2)
    log_handler.setLevel(logging.CRITICAL)
    logger.addHandler(log_handler)
    return logger
