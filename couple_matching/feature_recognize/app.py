# -*- coding: utf-8 -*-
from core_node import core_node_app


def train():
    node_id_list = range(3)
    for node_id in node_id_list:
        core_node_app.train_one(node_id)


def main():
    status = 'start'
    # train()
    print core_node_app.predict_one_by_one('cân nặng 50kg', node_id=1)
    sentences = ['chiều cao 1m75. cân nặng 60kg']
    total_feature_list = core_node_app.predict_all(sentences)
    for features in total_feature_list:
        print ('-' * 50)
        for feature in features:
            print(feature[0])
            print(feature[1])


if __name__ == '__main__':
    main()
